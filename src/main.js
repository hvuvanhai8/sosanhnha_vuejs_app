import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker';
import VueLazyload from 'vue-lazyload'
require('./modules/funtion_react_native');


Vue.config.productionTip = false;
// Vue.use(VueLazyload)

Vue.use(VueLazyload, {
  preLoad: 1.3,
  error:  '/images/noimage2.png',
  loading:  '/images/loading.png',
  attempt: 1
})
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

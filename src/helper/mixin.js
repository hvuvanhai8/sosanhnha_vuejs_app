export const MixinDemo =  {
    data: function () {
        return {
            message: 'hello',
            foo: 'abc'
        }
    },
    created: function(){
        this.hello();
    },
    methods:{
        hello: function () {
            alert('hello from mixin');
        }
    }
};
export const sayLogin = {
    data() {
        return {
            checked: false,
        }
    },
    methods: {
        setStatusLeftNav(value) {
            this.checked = value;
        },
    }
}

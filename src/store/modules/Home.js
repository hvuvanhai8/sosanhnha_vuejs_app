import {ApiGet} from "../../helper/api";

const Home=()=>{
    return{
        state:{
            home_news: null
        },
        getters:{
            GET_HOME_NEWS(state){
                return state.home_news
            }
        },
        mutations: {
            SET_HOME_NEWS(state,data){
                state.home_news = data;
            }
        },

        actions: {
            ACTION_HOME_NEWS({commit}){
                ApiGet('classifieds',{fields:'title,rewrite,price,picture,date,address,list_acreage'}).then(response=>{
                    commit('SET_HOME_NEWS',response.data)
                }).catch(error=>{
                    console.error(error);
                })
            }
        }
    }
};

export  default Home;
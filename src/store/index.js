import Vue from 'vue'
import Vuex from 'vuex'
import Category from "./modules/Category"
import {ApiGet, ApiPost,ApiDelete} from "../helper/api";
import Home from "./modules/Home";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        categories: [],
        cities: [
            {
                "id": 2,
                "name": "An Giang"
            },
            {
                "id": 3,
                "name": "Bà Rịa - Vũng Tàu"
            },
            {
                "id": 4,
                "name": "Bắc Giang"
            },
            {
                "id": 5,
                "name": "Bắc Kạn"
            },
            {
                "id": 6,
                "name": "Bạc Liêu"
            },
            {
                "id": 7,
                "name": "Bắc Ninh"
            },
            {
                "id": 8,
                "name": "Bến Tre"
            },
            {
                "id": 9,
                "name": "Bình Định"
            },
            {
                "id": 10,
                "name": "Bình Duong"
            },
            {
                "id": 11,
                "name": "Bình Phước"
            },
            {
                "id": 12,
                "name": "Bình Thuận"
            },
            {
                "id": 13,
                "name": "Cà mau"
            },
            {
                "id": 14,
                "name": "Cần Thơ"
            },
            {
                "id": 15,
                "name": "Cao Bằng"
            },
            {
                "id": 16,
                "name": "Đà Nẵng"
            },
            {
                "id": 17,
                "name": "Đăk Lăk"
            },
            {
                "id": 18,
                "name": "Đăk Nông"
            },
            {
                "id": 19,
                "name": "Điện Biên"
            },
            {
                "id": 20,
                "name": "Đồng Nai"
            },
            {
                "id": 21,
                "name": "Đồng Tháp"
            },
            {
                "id": 22,
                "name": "Gia Lai"
            },
            {
                "id": 23,
                "name": "Hà Giang"
            },
            {
                "id": 24,
                "name": "Hà Nam"
            },
            {
                "id": 25,
                "name": "Hà Nội"
            },
            {
                "id": 26,
                "name": "Hà Tĩnh"
            },
            {
                "id": 27,
                "name": "Hải Dương"
            },
            {
                "id": 28,
                "name": "Hải Phòng"
            },
            {
                "id": 29,
                "name": "Hậu Giang"
            },
            {
                "id": 30,
                "name": "Hồ Chí Minh"
            },
            {
                "id": 31,
                "name": "Hòa Bình"
            },
            {
                "id": 32,
                "name": "Hưng Yên"
            },
            {
                "id": 33,
                "name": "Khánh Hòa"
            },
            {
                "id": 34,
                "name": "Kiên Giang"
            },
            {
                "id": 35,
                "name": "Kon Tum"
            },
            {
                "id": 36,
                "name": "Lai Châu"
            },
            {
                "id": 37,
                "name": "Lâm Đồng"
            },
            {
                "id": 38,
                "name": "Lạng Sơn"
            },
            {
                "id": 39,
                "name": "Lào Cai"
            },
            {
                "id": 40,
                "name": "Long An"
            },
            {
                "id": 41,
                "name": "Nam Định"
            },
            {
                "id": 42,
                "name": "Nghệ An"
            },
            {
                "id": 43,
                "name": "Ninh Bình"
            },
            {
                "id": 44,
                "name": "Ninh Thuận"
            },
            {
                "id": 45,
                "name": "Phú Thọ"
            },
            {
                "id": 46,
                "name": "Phú Yên"
            },
            {
                "id": 47,
                "name": "Quảng Bình"
            },
            {
                "id": 48,
                "name": "Quảng Nam"
            },
            {
                "id": 49,
                "name": "Quảng Ngãi"
            },
            {
                "id": 50,
                "name": "Quảng Ninh"
            },
            {
                "id": 51,
                "name": "Quảng Trị"
            },
            {
                "id": 52,
                "name": "Sóc Trăng"
            },
            {
                "id": 53,
                "name": "Sơn La"
            },
            {
                "id": 54,
                "name": "Tây Ninh"
            },
            {
                "id": 55,
                "name": "Thái Bình"
            },
            {
                "id": 56,
                "name": "Thái Nguyên"
            },
            {
                "id": 57,
                "name": "Thanh Hóa"
            },
            {
                "id": 58,
                "name": "Thừa Thiên Huế"
            },
            {
                "id": 59,
                "name": "Tiền Giang"
            },
            {
                "id": 60,
                "name": "Trà Vinh"
            },
            {
                "id": 61,
                "name": "Tuyên Quang"
            },
            {
                "id": 62,
                "name": "Vĩnh Long"
            },
            {
                "id": 63,
                "name": "Vinh Phúc"
            },
            {
                "id": 64,
                "name": "Yên Bái"
            }
        ],
        districts: [],
        list_app_filter: null,
        access_token: null,
        user_info: null,
        configuration:null,
        client_id:null,
        client_token: null,
    },
    getters: {
        GET_CLIENT_TOKEN(state){
            return state.client_token;
        },
        GET_CLIENT_ID(state){
          return state.client_id;
        },
        GET_CATEGORIES(state) {
            return state.categories
        },
        GET_ACCESS_TOKEN(state) {
            return state.access_token;
        },
        GET_USER_INFO(state){
          return state.user_info;
        },
        GET_LOGIN(state){
          if(state.user_info!=null){
              return true;
          }else{
              return false;
          }
        },
        GET_CONFIGURATION(state){
          return state.configuration;
        },
        GET_CITIES(state){
          return state.cities;
        },
        GET_DISTRICTS(state) {
            return state.districts;
        },
        GET_LIST_APP_FILTER(state) {
            return state.list_app_filter;
        }

    },
    mutations: {
        SET_CLIENT_TOKEN(state,data){
            state.client_token = data;
        },
        SET_CLIENT_ID(state,data){
            state.client_id = data;
        },
        SET_ACCESS_TOKEN(state,data){
          state.access_token = data;
        },
        SET_USER_INFO(state,data){
          state.user_info = data;
        },
        SET_CATEGORIES(state, data) {
            state.categories = data;
        },
        SET_DISTRICT(state, data) {
            state.districts = data;
        },
        SET_LIST_APP_FILTER(state, data) {
            state.list_app_filter = data;
        },
        SET_CONFIGURATION(state,data){
            state.configuration = data;
        }
    },
    actions: {
        ACTION_CATEGORIES({commit}) {
            ApiGet('categories', {fields: 'id,name,rewrite,parent_id,type,name_guest', with: 'all'}).then(response => {
                commit('SET_CATEGORIES', response.data)
            }).catch(error => {
                console.error(error);
            })
        },
        ACTION_GET_DISTRICT({commit}, data) {
            commit('SET_DISTRICT', null);
            ApiGet('locations', data).then(res => {
                commit('SET_DISTRICT', res.data);
            }).catch(error => {
                console.log('get dis error:', error)
            })
        },
        ACTION_GET_LIST_APP_FILTER({commit}, data) {
            commit('SET_LIST_APP_FILTER', null);
            ApiGet('classifieds', data).then(res => {
                commit('SET_LIST_APP_FILTER', res.data);
            }).catch(error => {
                console.log('get dis error:', error)
            })
        },

    },
    modules: {
        Category: Category(),
        Home: Home()
    }
})


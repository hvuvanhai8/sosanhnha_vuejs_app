import Vue from 'vue'
import Router from 'vue-router'

import Index from '../views/Index'
import ChiTietCanHo from '../views/ChiTietCanHo'
import DuAn from '../views/DuAn'
import ChiTietDuAn from '../views/ChiTietDuAn'
import SoSanhDuAn from '../views/SoSanhDuAn'
import Search from '../views/Search'
import SwitchPage from "../views/SwitchPage"
import UserContact from "../views/UserContact"
import auth from "../middleware/auth"

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index
    },
    {
      path:'/listcontact',
      name:'UserContact',
      component:UserContact,
      meta:{
          middleware: auth
      }
    },
    {
      path: '/chitietcanho',
      name: 'ChiTietCanHo',
      component: ChiTietCanHo
    },
    {
      path: '/duan',
      name: 'DuAn',
      component: DuAn
    },
    {
      path: '/chitietduan',
      name: 'ChiTietDuAn',
      component: ChiTietDuAn
    },
    {
      path: '/sosanhduan',
      name: 'SoSanhDuAn',
      component: SoSanhDuAn
    },
    {
      path: '/search',
      name: 'Search',
      component: Search
    },
    // {
    //   path: '/*-cla:id',
    //   name: 'ChiTietCanHo',
    //   component: ChiTietCanHo
    // },
    {
      path: '/:slug',
      name: 'SwitchPage',
      component: SwitchPage
    },
    {
      path: '/*/:slug',
      name: 'SwitchPageV2',
      component: SwitchPage
    },
  ]
})

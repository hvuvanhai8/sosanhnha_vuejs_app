console.log('load javascript');
window.numCallMicrophone = 0;

window.vue_handle = {};
// var voiceRecognitionRecognizing;

window.loginFacebook = function () {
    postReactNativeWebViewMessage({
        ref: 'facebookButton',
        action: 'facebookLogin',
    });
};

window.loginGoogle = function () {
    postReactNativeWebViewMessage({
        ref: 'gooleButton',
        action: 'googleLogin',
    });
};

window.fcmStart = function () {
    postReactNativeWebViewMessage({
        ref: 'fcmStartButton',
        action: 'fcmStart'
    })
}

window.fcmToken = function () {
    postReactNativeWebViewMessage({
        ref: 'fcmTokenButton',
        action: 'fcmToken'
    })
}

window.onesignalStart = function () {
    postReactNativeWebViewMessage({
        action: 'oneSignalStart',
    });
};

window.onesignalSendTags = function () {
    postReactNativeWebViewMessage({
        action: 'oneSignalSendTags',
        data: {
            app: 'vnp-web-app',
        },
    });
};

window.startLocation = () => {
    postReactNativeWebViewMessage({
        action: 'locationStart',
    });
};

window.updateLocation = function () {
    postReactNativeWebViewMessage({
        action: 'locationUpdate',
    });
};

window.startWatchingLocation = function () {
    postReactNativeWebViewMessage({
        action: 'locationStartWatching',
    });
};

window.stopWatchingLocation = function () {
    postReactNativeWebViewMessage({
        action: 'locationStopWatching',
    });
};

window.uploadGallery = function () {
    postReactNativeWebViewMessage({
        action: 'uploadGallery',
        data: {
            mediaType: 'photo',
            multiple: true,
            maxWidth: 1000,
            maxHeight: 1000,
            maxSize: 1048576
        }
    });
};

window.uploadCamera = function () {
    postReactNativeWebViewMessage({
        action: 'uploadCamera',
        data: {
            mediaType: 'photo',
            cropping: true,
            width: 300,
            height: 300,
            maxWidth: 1000,
            maxHeight: 1000,
            maxSize: 1048576
        }
    });
};



window.voiceRecognitionAvailable = function (id_input) {
    // alert(id_input);
    // if (window.voiceRecognitionRecognizing) return;
    // alert('voiceRecognitionAvailable');
    var idInput = id_input || 'voiceRecognitionAvailableButton';
    postReactNativeWebViewMessage({
        ref: idInput,
        action: 'voiceRecognitionAvailable'
    });
}

window.voiceRecognitionRecognizing = function (id_input) {
    var idInput = id_input || 'voiceRecognitionRecognizingButton';
    postReactNativeWebViewMessage({
        ref: idInput,
        action: 'voiceRecognitionRecognizing'
    });
}

window.voiceRecognitionStart = function (id_input) {
    // if (voiceRecognitionRecognizing) return;
    var idInput = id_input || 'voiceRecognitionStartButton';
    postReactNativeWebViewMessage({
        ref: idInput,
        action: 'voiceRecognitionStart',
        data: {
            locale: 'vi'
        }
    });
}

window.voiceRecognitionStop = function(id_input) {
    var idInput = id_input || 'voiceRecognitionStopButton';
    postReactNativeWebViewMessage({
        ref: idInput,
        action: 'voiceRecognitionStop'
    });
};

window.voiceRecognitionCancel =  function (id_input) {
    var idInput = id_input || 'voiceRecognitionCancelButton';
    postReactNativeWebViewMessage({
        ref: idInput,
        action: 'voiceRecognitionCancel'
    });
};

window.postReactNativeWebViewMessage = function ({ref, action, data}) {
    if (!window.ReactNativeWebView
        || !window.ReactNativeWebView.postMessage
        || typeof window.ReactNativeWebView.postMessage !== 'function') {
        console.warn('can not call postMessage of window.ReactNativeWebView');
        return;
    }

    const json = JSON.stringify({ref, action, data});
    alert('json:'+json);
    console.log('call postMessage of window.ReactNativeWebView: ', json);
    window.ReactNativeWebView.postMessage(json);
};

window.ReactWebApp = function () {

    this.postMessage = function (message) {
        alert('message');
        var messageJSON = JSON.parse(message);
        var data = messageJSON.data || {};
        var objerror = data.error || {};
        var errormsg = objerror.message || null;
        var straction = messageJSON.action || null;
        alert('straction:',straction);

        /*
        if(errormsg !== null){
            document.getElementById('content_voice').innerHTML = straction+'=>'+errormsg;
        }
        //*/
        switch (straction) {
            case "facebookLogin":
                alert('face login');
                window.location.href = '/dang-nhap.aspx?type=facebook_app_vnp&message=' + encodeURIComponent(message) + '&url=Lz9zdGF0dXNfbG9naW49c3VjY2Vzcw==';
                break;
            case "googleLogin":
                alert('google login')
                window.location.href = '/dang-nhap.aspx?type=google_app_vnp&message=' + encodeURIComponent(message) + '&url=Lz9zdGF0dXNfbG9naW49c3VjY2Vzcw==';
                break;
            case "voiceRecognitionSpeechResults":
                window.vue_handle.voiceRecognitionSpeechResults(messageJSON);
                // var listResult = data.value || [];
                // var input_id = messageJSON.ref || 'khongtontai';
                // voiceRecognitionStop(input_id);
                // var htmlResult = '</ul>';
                // $.each(listResult, function( index, value ) {
                //     if(input_id == "autocomplete"){
                //         document.getElementById('content_voice').innerHTML = value;
                //         window.location.href='/search?keyword='+value;
                //         return true;
                //     }else{
                //         $("#"+input_id).val(value);
                //         voiceRecognitionStop(input_id);
                //         voiceRecognitionCancel(input_id);
                //     }
                //     htmlResult += '<li>'+value+'</li>';
                // });
                // htmlResult += '</ul>';

                break;
            case "voiceRecognitionRecognizing":
                // voiceRecognitionRecognizing = data.recognizing || false;
                window.vue_handle.voiceRecognitionRecognizing(messageJSON);
                break;
            case "voiceRecognitionAvailable":
                window.vue_handle.voiceRecognitionAvailable(messageJSON);
                // voiceRecognitionStart('input_id');
                var VoiceAvailable = data.available || false;
                var input_id = messageJSON.ref || 'khongtontai';
                if(VoiceAvailable === false){
                    alert('Khong xin duoc quyen');
                    // document.getElementById('content_voice').innerHTML = "Điện thoại không hỗ trợ giọng nói!";
                }else{
                    alert('Xin duoc quyen');
                    voiceRecognitionStart('input_id');
                }
                break;
            case "fcmToken":
                window.vue_handle.fcmToken(messageJSON);
                // var fcmToken = data.token || null;
                // if(fcmToken !== null){
                //     api_chat_notification.token = fcmToken;
                //     /*
                //      alert(JSON.stringify(api_chat_notification));
                //      //*/
                //     $.ajax({
                //         type: "POST",
                //         url: "<?=env("CHAT_API")?>/notifications",
                //         data: api_chat_notification,
                //         success:function(){
                //         },
                //         error: function (request, status, error) {
                //         }
                //     });
                // }
                break;
            case "voiceRecognitionSpeechError":
                window.vue_handle.voiceRecognitionSpeechError(messageJSON);
                // if(voiceRecognitionRecognizing) break;
                // numCallMicrophone++;
                // var htmlLoading = document.getElementById('content_voice').innerHTML;
                // if(numCallMicrophone < 3){
                //     voiceRecognitionCancel(input_id);
                //     setTimeout(function(){
                //         var input_id = messageJSON.ref || 'khongtontai';
                //         document.getElementById('content_voice').innerHTML = "Thử lại lần "+numCallMicrophone;
                //         document.getElementById('content_voice').innerHTML = htmlLoading;
                //         voiceRecognitionStart(input_id);
                //     }, numCallMicrophone*1500);
                // }else{
                //     numCallMicrophone = 0;
                //     //voiceRecognitionStop(input_id);
                //     voiceRecognitionCancel(input_id);
                //     document.getElementById('content_voice').innerHTML = "Nhận diện giọng nói không thành công!";
                // }

                break;
            case "uploadGallery":
                window.vue_handle.uploadGallery(messageJSON);
            case "uploadCamera":
                window.vue_handle.uploadCamera(messageJSON);
                // returnUploadVNP(message);
                break;
        }
    }
}

window.ReactWebApp = new ReactWebApp();